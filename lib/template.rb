module Template
  TEMPLATE_PATH = File.expand_path("template.txt", "vendor")

  def self.get
    File.new(TEMPLATE_PATH, 'r').readlines[0].strip
  rescue Errno::ENOENT
    abort("#{TEMPLATE_PATH} not found")
  end

end
