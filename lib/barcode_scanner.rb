require "bundler/setup"
Bundler.require

require_relative 'barcode_aligner'
require_relative 'sequence_reader'
require_relative 'sequence'
require_relative 'printer'
require_relative 'template'

module BarcodeScanner
  def self.process(file_or_dir_path)
    template = Template.get
    printer = Printer.new
    SequenceReader.new(file_or_dir_path, template).each_filename_and_sequence do |filename, sequence|
      printer.write filename, sequence.pattern
    end
  end
end
