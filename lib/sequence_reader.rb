class SequenceReader
  attr_reader :file_or_dir_path, :template

  def initialize(file_or_dir_path, template)
    @file_or_dir_path = File.join("vendor", file_or_dir_path)
    @template = template
  end

  def each_filename_and_sequence
    filenames.each do |filename|
      next if filename == '.' || filename == '..'
      file_path = File.join(file_or_dir_path, filename)
      yield filename, Sequence.new(raw_sequence_from_file_path(file_path), template)
    end
  end

  private

  def filenames
    if File.file?(file_or_dir_path)
      [file_or_dir_path]
    else
      Dir.entries(file_or_dir_path)
    end
  rescue Errno::ENOENT
    abort("#{file_or_dir_path} directory not found\n")
  end

  def raw_sequence_from_file_path(file_path)
    File.new(file_path, 'r').readlines[0].delete("N").strip
  rescue NoMethodError
    nil
  end
end
