class BarcodeAligner < NeedlemanWunschAligner
  MATCHING_NUCLEOTIDE = 2
  NOT_MATCHED = -2
  GAP_PENALTY = -3
  MATCHING_STAR = 1

  def compute_score(left_el, top_el)
    if left_el == '*' || top_el == '*'
      return MATCHING_STAR
    end
    left_el == top_el ? MATCHING_NUCLEOTIDE : NOT_MATCHED
  end

  def gap_indicator
    '.'
  end

  def default_gap_penalty
    GAP_PENALTY
  end

end
