class Sequence
  attr_reader :template, :raw_sequence
  def initialize(raw_sequence, template)
    @raw_sequence = raw_sequence
    @template = template
  end

  def pattern
    return nil if !raw_sequence
    raw_variables, raw_constants = get_alignment
    raw_variables = substitute_raw_variables(raw_variables, raw_constants)
    variables, constants = get_nucleotides(raw_variables, regex_from_constants(raw_constants))
    variables, constants = kill_duplicate_variables(variables), kill_duplicate_constants(constants)
    variables_mutations, constants_mutations = get_variables_mutations(variables), get_constants_mutations(constants)
    return contacenate_patterns(variables, constants, variables_mutations, constants_mutations)
  end

  private

  def get_alignment
    if sequence = get_sequence
      alignment = BarcodeAligner.new(sequence, template)
      return alignment.get_optimal_alignment[0].join(''), alignment.get_optimal_alignment[1].join('').gsub(/\.(?=\.*\*)/, '*')
    end
  end

  def get_sequence
    if sequence = raw_sequence.match(/TTC{1,2}GA{1,2}TCT(.{0,100})GAGAGATCGG/)
      sequence[1]
    end
  end

  #Returns regex created from raw constants by substituting every triplet by '.' and grouping them
  def regex_from_constants(raw_constants)
    raw_constants.split(/(\*+)/).drop(1).map{ |x| '(' + x.gsub(/./, '.') + ')'}.join('')
  end

  def get_mutations(raw_variables, raw_constants, triplets_pattern)
    variables, constants = get_triplets(raw_variables, triplets_pattern)
    return get_variables_mutations(variables), get_constants_mutations(constants)
  end

  def get_variables_mutations(variables)
    variables = deep_copy(variables)
    variables.map.with_index do |variable, i|
      normal_length = (i == 0) ? 3 : 2
      if variable.length > normal_length
        variable[normal_length] = '.'
      elsif variable.length < normal_length
        variable[normal_length] = '.'
      end
      variable.gsub(/[\w]/, ' ').gsub(/[\.\*]/, '^')
    end
  end

  def get_constants_mutations(constants)
    default_constants = template.split(/\*+/).drop(1)
    constants = deep_copy(constants)
    constants.zip(default_constants).map do |constant, default_constant|
      constant = constant.split("").zip(default_constant.split("")).map do |constant_char, default_constant_char|
        if constant.length > 3
          constant[2] = '.'
        elsif constant.length < 3
          constant[-1] = '.'
        end
        constant_char == default_constant_char ?  constant_char : '.'
      end
      constant.join('').gsub(/\w/, ' ').gsub(/\./, 'v')
    end
  end

  def get_nucleotides(raw_variables, triplets_pattern)
    triplets = raw_variables.match(/#{triplets_pattern}/).captures
    return triplets.select.each_with_index { |_, i| i.even?}, triplets.select.each_with_index { |_, i| i.odd?}
  end

  def kill_duplicate_variables(variables)
    variables.each_with_index do |variable, i|
      normal_length = (i == 0) ? 3 : 2
      if variable.length > normal_length
        variable.gsub!(/(\w)\1/, '\1')
      end
    end
  end

  def kill_duplicate_constants(constants)
    constants.each_with_index do |constant, i|
      if constant.length > 3
        constant.gsub!(/(\w)\1/, '\1')
      end
    end
  end

  #Changes constants mutations in variables string(marked as '.') to their constant nucleotide equivalent from constants string
  def substitute_raw_variables(raw_variables, raw_constants)
    raw_variables.split('').each_with_index.map{ |c, i| c == '.' ? raw_constants[i] : c }.join('')
  end

  def contacenate_patterns(variables, constants, variables_mutations, constants_mutations)
    lines = Array.new(5, "")
    variables.length.times do |i|
      varlen = variables[i].length
      constlen = constants[i].length
      space = i.odd? ? " " : ""
      lines[0] += space + " "*varlen + constants_mutations[i]
      lines[1] += space + "N"*varlen + constants[i]
      lines[2] += space + variables[i] + "*"*constlen
      lines[3] += space + variables_mutations[i] + " "*constlen
    end
    lines[4] = "Barcode: " + variables.join("") + "\n"
    lines.join("\n")
  end

  #It's like Deep Ones, only more horrendous
  def deep_copy(obj)
    Marshal.load(Marshal.dump(obj))
  end

end
