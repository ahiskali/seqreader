class Printer
  OUTPUT_PATH = File.expand_path("output.txt", "vendor")

  attr_reader :file

  def initialize
    @file = File.new(OUTPUT_PATH, 'w')
  end

  def write(filename, pattern)
    file.write output_for_pattern(filename, pattern)
  end

  private

  def output_for_pattern(filename, pattern)
    if pattern
      filename + ":\n" + pattern + "\n"
    else
      "#{filename}: pattern not found\n\n"
    end
  end
end
