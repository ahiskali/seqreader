class RawSequence
  attr_reader :template, :raw_sequence, :raw_variables, :raw_constants

  def initialize(raw_sequence, template)
    @raw_sequence = raw_sequence
    @template = template

  end

  def pattern
    return nil if !raw_sequence
    @raw_variables, @raw_constants = get_alignment
    raw_variables = substitute_raw_variables # SKETCHY
    variables, constants = triplets(raw_variables, regex_from_constants(raw_constants))
    Triplets.new(variables, constants).pattern
  end

  private

  def get_alignment
    if sequence = get_sequence
      alignment = BarcodeAligner.new(sequence, template)
      return alignment.get_optimal_alignment[0].join(''), alignment.get_optimal_alignment[1].join('').gsub(/\.(?=\.*\*)/, '*')
    end
  end

  def get_sequence
    if sequence = raw_sequence.match(/TTC{1,2}GA{1,2}TCT(.{0,100})GAGAGATCGG/)
      sequence[1]
    end
  end

  #Changes constants mutations in variables string(marked as '.') to their constant nucleotide equivalent from constants string
  def substitute_raw_variables(raw_variables, raw_constants)
    raw_variables.split('').each_with_index.map{ |c, i| c == '.' ? raw_constants[i] : c }.join('')
  end

  def triplets(raw_variables, triplets_pattern)
    var_and_const_triplets = raw_variables.match(/#{triplets_pattern}/).captures
    return var_and_const_triplets.select.each_with_index { |_, i| i.even?}, var_and_const_triplets.select.each_with_index { |_, i| i.odd?}
  end

end
