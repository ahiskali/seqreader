require "spec_helper"

RSpec.describe Sequence do
  describe '#get_variables_mutations' do
    let(:template) { SequenceReader.new("vendor/seqs/input.txt").template }
    let(:sequence) { Sequence.new("vendor/seqs/input.txt", template) }

    subject { sequence.get_variables_mutations(variables) }

    describe 'length' do
      context '3 nucleotides' do
        let(:variables) { %w(ABC) }
        it { is_expected.to eq(['   ']) }
      end

      context '4 nucleotides' do
        let(:variables) { %w(ABCD) }
        it { is_expected.to eq(['   ^']) }
      end
    end
  end

end
